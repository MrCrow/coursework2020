﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceaneLoading : MonoBehaviour
{
    public int sceneID;
    public Image loadingImg;
    public Text ProgressText;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AsyncLoad());
    }
    IEnumerator AsyncLoad()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneID);
        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            loadingImg.fillAmount = progress;
            ProgressText.text = string.Format("{0:0}%", progress * 100);
            yield return null; 
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
