﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public Sprite red;
    public Sprite green;
    private SpriteRenderer check;
    public bool checkint;
    // Start is called before the first frame update
    void Start()
    {
        check = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            check.sprite = green;
            checkint = true;
        }
    }
}
