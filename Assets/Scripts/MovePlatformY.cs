﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatformY : MonoBehaviour
{
    public float spacing = 1.92f;
    public float nus = 1f;
    float direction = 1;
    float minX, maxX;
    // Start is called before the first frame update
    void Start()
    {
        minX = transform.position.x;
        maxX = minX + nus * spacing;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 currPos = transform.position;
        if (currPos.x >= maxX)
        {
            direction = -1;
        }
        if (currPos.x <= minX)
        {
            direction = 1;
        }
        transform.Translate(new Vector3(direction * 2f * Time.deltaTime, direction * 2f * Time.deltaTime, 0));
    }
}
