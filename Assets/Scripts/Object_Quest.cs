﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Quest : MonoBehaviour
{
    public Qwest QEvent;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            QEvent.END_Quest1 = true;
            Destroy(gameObject);

        }
    }
}