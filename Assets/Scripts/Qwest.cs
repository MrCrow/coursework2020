﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Qwest : MonoBehaviour
{
    public bool Quest1;
    public GameObject Text1;
    public bool END_Quest1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (END_Quest1 == false)
        {
            if (Quest1 == true)
            {
                Text1.SetActive(true);
            }
            else
            {
                Text1.SetActive(false);
            }
        }
        else
        {
            Text1.SetActive(false);
        }
    }
}
