﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog_Next : MonoBehaviour
{
    public GameObject Text1;
    public GameObject Text2;
    public GameObject OQuest;
    private bool isText1 = true;
    public NPS_Task2 npc_taskScript;
    public bool Fin;

    // Use this for initialization
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (isText1 == true)
            {
                isText1 = false;
            }
            else
            {
                if (Fin == false)
                {
                    isText1 = true;
                    OQuest.SetActive(true);
                    npc_taskScript.ENDialog = true;
                }
                else
                {
                    isText1 = true;
                    npc_taskScript.FIN_Dialog = true;
                    
                }
            }
        }
        if (isText1 == true)
        {
            Text1.SetActive(true);
            Text2.SetActive(false);
        }
        else
        {
            Text1.SetActive(false);
            Text2.SetActive(true);
        }
    }
}
