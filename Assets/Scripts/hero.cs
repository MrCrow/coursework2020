﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class hero : MonoBehaviour
{
    Rigidbody2D rb;
    Animator anim;
    public AudioSource walking;
    public AudioSource jump;
    private bool isFacingRight = true;
    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask WhatIsGround;
    public float jumpForce;
    private int extrJamps;
    public int extaJumpsValue;

    void Start()
    {
        extrJamps = extaJumpsValue;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
 
        if (isGrounded == true)
        {
            extrJamps = extaJumpsValue;
        }
        if (Input.GetKeyDown(KeyCode.Space) && extrJamps > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extrJamps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extrJamps == 0 && isGrounded == true)
        {
            anim.SetBool("Ground", false);
            rb.velocity = Vector2.up * jumpForce;
            jump.Play();
        }
  
        if (Input.GetAxis("Horizontal") == 0)
        {
            anim.SetInteger("Number", 0);
            walking.Pause();
        }
        
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A  ))
        {
         
            anim.SetInteger("Number", 1); 
         
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {  
         
            anim.SetInteger("Number", 1);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetInteger("Number", 1);
        }
        if ((Input.GetKey(KeyCode.LeftShift)))
        {
            anim.SetInteger("Number", 2);
           
        }

    }



    private void Flip()
    {

        //меняем направление движения персонажа
        isFacingRight = !isFacingRight;
        //получаем размеры персонажа
        transform.Rotate(0f, 180f, 0f);
        //зеркально отражаем персонажа по оси Х

    }
    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, WhatIsGround);
        anim.SetBool("Ground", isGrounded);

       
        float move = Input.GetAxis("Horizontal");
        if (move > 0 && !isFacingRight)
        {
            walking.Play();
            //отражаем персонажа вправо
            Flip();
            
        }
        //обратная ситуация. отражаем персонажа влево
        else if (move < 0 && isFacingRight)
        {
            walking.Play();
            Flip();
        }
      
        rb.velocity = new Vector2(Input.GetAxis("Horizontal") * 4f, rb.velocity.y);
      
        if ((Input.GetKey(KeyCode.LeftShift)))
        {
            walking.Play();
            rb.velocity = new Vector2(Input.GetAxis("Horizontal") * 7f, rb.velocity.y);
        }

    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "MovePlatform")
        {
            transform.parent = col.transform;
        }
        
    }
    public void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "MovePlatform")
        {
            transform.parent = null;
        }
    }
}
