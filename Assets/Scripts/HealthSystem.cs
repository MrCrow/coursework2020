﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthSystem : MonoBehaviour
{
    public int health;
    public int coin;
    public int numberOfLives;

    public Image[] lives;
    public Sprite fullLive;
    public Sprite emptyLive;
    hero heros;
    public Vector3 respawnpont;
    // Start is called before the first frame update
    void Start()
    {
        heros = GetComponent<hero>();
    }
   
    // Update is called once per frame
    void Update()
    {
        if (health < 1)
        {


            transform.position = respawnpont;
            health =5;
        }
        if(health> numberOfLives)
        {
            health = numberOfLives;
        }

        for (int i = 0; i < lives.Length; i++)
        {
            if(i< health)
            {
                lives[i].sprite = fullLive;
            }
            else
            {
                lives[i].sprite = emptyLive;
            }

            if(i< numberOfLives)
            {
                lives[i].enabled = true;
            }
            else
            {
                lives[i].enabled = false;
            }
        }
    }
         void OnTriggerEnter2D(Collider2D cols)
        {
        if (cols.tag == "CheckPoint")
        {
            respawnpont = cols.transform.position;
        } 
       

        }           
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            health--;
        }
       
      if(col.gameObject.tag == "Dialog")
        {
            coin++;
            col.gameObject.SetActive(false);
        }
        if (col.gameObject.tag == "health")
        {
            health++;
            col.gameObject.SetActive(false);
        }
  
        if (col.gameObject.tag == "DieColaider" )
        {
            health = 0;
        }
    }
}
