﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPS_Task2 : MonoBehaviour
{
    public bool ENDialog;
    public GameObject Dialog1;
    public GameObject Dialog2;
    public Qwest QE;
    public bool FIN_Dialog;
    public bool onhide;
    public Transform DoorObj;
    public Transform OpenPoint;
    public int SPEED;
    public bool Open = false;



    // Start is called before the first frame update
    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        if (QE.END_Quest1 == true)
        {
            FIN_Dialog = true;
        }
        if(ENDialog== true)
        {
            Time.timeScale = 1;
            QE.Quest1 = true;
            Dialog1.SetActive(false);
        }
        if (FIN_Dialog == true)
        {
            Time.timeScale = 1;
            QE.Quest1 = false;
            Dialog1.SetActive(false);
           
        }
      
    }
    public void Openens()
    {
        DoorObj.position = Vector2.Lerp(DoorObj.position, OpenPoint.position, SPEED * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (FIN_Dialog == false)
            {
                Time.timeScale = 0;
                
                if (QE.END_Quest1 == false)
                {
                    Dialog1.SetActive(true);
                  
                }
               
            }
            else
            {

                if (onhide == false)
                {
                    Dialog2.SetActive(true);
                    
                    onhide = true;
                   
                    if (onhide == true)
                    {
                        Destroy(Dialog2, 5f);
                        Openens();
                    }

                }
            }
        }
    }
}
